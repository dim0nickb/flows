N = 99;
roben = dlmread('roben_Psi.data', ' ', [0 0 N N]);
figure; surf(roben);
figure; contour(roben);

vortex = dlmread('vortex_Psi.data', ' ', [0 0 N N]);
figure; surf(vortex);
figure; contour(vortex);

psi = dlmread('psi_Surface.data', ' ', [0 0 N N]);
figure; surf(psi);
figure; contour(psi);