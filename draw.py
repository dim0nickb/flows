import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import csv
n = 200;
z = np.zeros((n, n))
with open('C:/Users/wdUser/Documents/flows/flows/bin/Debug/biharm_Surface.data','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=' ')
    i = 0
    for row in plots:
        j = 0
        for item in row:
            z[i][j] = float(item) 
            j = j+1
        i = i+1
z = z.transpose();
lb = -5.0;
rb =  5.0;
deltaX = (rb-lb) / n;
db = -3.0;
ub =  3.0;
deltaY = (ub-db) / n;
x = np.arange(lb, rb, deltaX)
y = np.arange(db, ub, deltaY)
X, Y = np.meshgrid(x, y)
Z = z

fig, ax = plt.subplots()
ax.set_aspect('equal')
CS = ax.contour(X, Y, Z, 100)
ax.clabel(CS, inline=1, fontsize=10)
ax.set_title('Psi(x)')

fig = plt.gcf()
fig.set_size_inches(15, 15)
plt.show()
fig.savefig('C:/temp/res_3.png')