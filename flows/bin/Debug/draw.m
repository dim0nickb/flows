clear all;
N = 199;

% roben = dlmread('roben_Surface.data', ' ', [0 0 N N]);
% figure; surf(roben);
% figure; contour(roben);
% 
% vortex = dlmread('vortex_Surface.data', ' ', [0 0 N N]);
% figure; surf(vortex);
% figure; contour(vortex);

psi = dlmread('psi_Surface.data', ' ', [0 0 N N]);

psi = (psi');
Z = psi;
[X,Y] = meshgrid(-5:0.05:4.95,-3:0.03:2.98);
[DX,DY] = gradient(Z,.2,.12);
t = 0:0.1:2*pi;
x = 4.6*cos(t);
y = 2.6*sin(t);
figure
contour(X,Y,Z,30)
hold on
quiver(X,Y,DX,DY)
plot(x,y,'b-')
hold off

figure; surf(psi);
% figure; 
% hold on;
% contour(psi, 30);
% contour(psi);
% hold off;

% psi = dlmread('psiSimple_Surface.data', ' ', [0 0 N N]);
% figure; surf(psi);
% figure; contour(psi);
