﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace flows
{
	class Program
	{
		static int procCount = Environment.ProcessorCount;
		static List<double> psix_layers_values = new List<double>();
		static List<double> psiy_layers_values = new List<double>();
		static List<double> psi_layers_values = new List<double>();
		static view.form view;
		static void Main(string[] args)
		{
			DateTime now = DateTime.Now;
			string dirName = now.Year + "_" + now.Month + "_" + now.Day + "-" + now.Hour + "_" + now.Minute;

			System.Random rnd = new Random();

			for (int i = 0; i < math.settings.zN - math.settings.zN_inner; i++)
			{
				double phi = 2.0 * Math.PI * i / (math.settings.zN - math.settings.zN_inner);
				math.settings.zx.Add(math.contour.x(phi) * 1.2 + rnd.Next(3) * 1.0 / 10);
				math.settings.zy.Add(math.contour.y(phi) * 1.2 + rnd.Next(3) * 1.0 / 10);
			}

			//for (int i = 0; i < math.settings.zN_inner; i++)
			//{
			//	double phi = 2.0 * Math.PI * i / math.settings.zN_inner;
			//	math.settings.zx.Add(math.bone.x(phi) * 0.9);
			//	math.settings.zy.Add(math.bone.y(phi) * 0.9);
			//}

			//math.settings.zx.Add(0.4);
			//math.settings.zx.Add(0.35);
			//math.settings.zx.Add(0.4);

			//math.settings.zy.Add(1.2);
			//math.settings.zy.Add(2.0);
			//math.settings.zy.Add(2.8);

			//math.settings.zx.Add(-0.0);
			//math.settings.zx.Add(-0.05);
			//math.settings.zx.Add(-0.0);

			//math.settings.zy.Add(1.2);
			//math.settings.zy.Add(2.0);
			//math.settings.zy.Add(2.8);
			//double dx = -1.5;
			//double dy = -1;
			//math.settings.zx.Add(0.2 + dx); math.settings.zy.Add(1.2 + dy);
			//math.settings.zx.Add(0.2 + dx); math.settings.zy.Add(1.8 + dy);

			//math.settings.zx.Add(1.5 + dx); math.settings.zy.Add(1.9 + dy);
			//math.settings.zx.Add(1.0 + dx); math.settings.zy.Add(2.4 + dy);

			//math.settings.zx.Add(1.8 + dx); math.settings.zy.Add(2.8 + dy);
			//math.settings.zx.Add(1.2 + dx); math.settings.zy.Add(2.8 + dy);

			for (int i = 0; i < math.settings.zN; i++)
			{
				double phi = 2.0 * Math.PI * i / (math.settings.zN);
				math.settings.zx_minus.Add(math.contour.x(phi) * 0.9 - rnd.Next(3) * 1.0 / 10);
				math.settings.zy_minus.Add(math.contour.y(phi) * 0.9 - rnd.Next(3) * 1.0 / 10);
			}

			for (int i = 0; i < math.settings.sN; i++)
			{
				double phi = 2.0 * Math.PI * i / math.settings.sN;
				math.settings.sx.Add(math.contour.x(phi));
				math.settings.sy.Add(math.contour.y(phi));
			}

			//Console.WriteLine(math.mu.calcM(2, -.3, 0.2));
			//Console.ReadKey();
			//return;
			bool pass = false;

			refrenSolo();
			Console.WriteLine("procCount: " + procCount);
			Console.WriteLine("go");
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			/*
			Func<double, double, double> f = (x, y) => math.mu.calcM(3, x, y) * math.mu.calcM(8, x, y);
			double v = math.integral2.calc(
				math.settings.x1,
				math.settings.x2,
				math.settings.y1,
				math.settings.y2,
				f,
				math.settings.needCalcPointInside
			);
			System.Diagnostics.Debug.Assert(!double.IsNaN(v));
			Console.WriteLine(v.ToString());
			Console.ReadKey();
			return;
			/**/
			Console.WriteLine("processing biharm");
			calcBiharm(math.f.calc, false);
			/*
			Console.WriteLine("processing Roben");
			Func<double, double, double> Roben = calcRoben(pass);
			Console.WriteLine("processing SelfVortex");
			Func<double, double, double> SelfVortex = calcSelfVortex(Roben, pass);
			Console.WriteLine("processing Psi");
			Func<double, double, double> Psi = calcPsi(SelfVortex);
			/**/
			/*
			Console.WriteLine("processing SelfVortex");
			Func<double, double, double> SelfVortex = calcSelfVortex((x,y) => -2, false);
			/*
			Console.WriteLine("processing PsiSimple");
			calcPsiSimple();
			sw.Stop();
			Console.WriteLine("time: " + sw.ElapsedMilliseconds + " ms, " + sw.ElapsedMilliseconds/1000 + "sec.");
			/**/

			coupleSolo();
			
			view = new view.form();
			view.onReady += view_onReady;
			Application.Run(view);
			/**/
			Console.Write("press any key...");
			Console.ReadKey();
		}

		private static void CalcSurf(Func<double, double, double> Psi, double minx, double miny, string prefix, int steps)
		{
			double minPsi=0;
			double maxPsi=0;
			/*
			math.integral.saveValues(prefix + "values.txt");
			math.integral.resetValues();
			/**/

			Console.WriteLine("processing " + prefix + "Surface with " + steps + "x" + steps + " matrix:");
			StringBuilder sb = new StringBuilder();
			double px = minx;
			for (int i = 0; i < steps; i++)
			{
				double py = miny;
				
				for (int j = 0; j < steps; j++)
				{

					Func<double, double, double> d = (px_, py_) => {
						double psi = Psi(px_, py_);

						if (psi > maxPsi)
						{
							maxPsi = psi;
						}
						if (psi < minPsi)
						{
							minPsi = psi;
						}
						return psi;
					};
					double psi_ = d(px, py);
					if (j < steps - 1)
					{
						sb.Append(psi_.ToString().Replace(',', '.') + " ");
					}
					else
					{
						sb.Append(psi_.ToString().Replace(',', '.'));
					}
					py += Math.Abs(miny * 2.0) / steps;
				}
				px += Math.Abs(minx * 2.0) / steps;
				Console.CursorLeft = 0;
				Console.Write("{0}%", (int)(i * 1.0 / steps * 100));
				if (i < steps - 1)
				{
					sb.AppendLine();
				}
			}

			Console.CursorLeft = 0;
			Console.WriteLine("100%");
			System.IO.File.WriteAllText(prefix + "Surface.data", sb.ToString());
			Console.WriteLine("min: {0}, max: {1}", minPsi, maxPsi);
		}

		/// <summary>
		/// Решаем задачу Робена
		/// </summary>
		/// <returns></returns>
		private static Func<double, double, double> calcRoben(bool pass)
		{
			Console.WriteLine("processing Ax=b:");
			string prefix = "roben_";
			math.Matrix cm;
			if (!pass)
			{
				string B = "";
				StringBuilder sb = new StringBuilder();
				for (int k = 0; k < math.settings.zN; k++)
				{
					
					for (int p = 0; p < math.settings.zN; p++)
					{
						Func<double, double> a = (t) =>
							math.alpha_minus.calcM(k, math.contour.x(t), math.contour.y(t)) *
							math.alpha_minus.calcM(p, math.contour.x(t), math.contour.y(t));
						double v = math.integral.calc(0, 2 * Math.PI, a);
						if (p < math.settings.zN - 1)
						{
							sb.Append(v + " ");
						}
						else
						{
							sb.Append(v);
						}
					}

					Func<double, double> b = (t) => math.alpha_minus.calcM(k, math.contour.x(t), math.contour.y(t));
					double valB = math.integral.calc(0, 2 * Math.PI, b);
					if (k < math.settings.zN - 1)
					{
						sb.AppendLine();
						B += valB + " ";
					}
					else
					{
						B += valB;
					}
					Console.CursorLeft = 0;
					Console.Write("{0}%", 100 * k / math.settings.zN);

				}
				Console.CursorLeft = 0;
				Console.WriteLine("100%");

				System.IO.File.WriteAllText(prefix + "A.data", sb.ToString());
				System.IO.File.WriteAllText(prefix + "B.data", B.ToString());

				math.Matrix A = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "A.data"));
				math.Matrix B_vector = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "B.data"));
				cm = A.SolveWith(math.Matrix.Transpose(B_vector));
				System.IO.File.WriteAllText(prefix + "cm.data", cm.ToString());
			}
			else
			{
				cm = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "cm.data"));
			}

			Func<double, double, double> phiN = (x, y) =>
			{
				double sum = 0;
				for (int i = 0; i < math.settings.zN; i++)
				{
					sum += cm[i, 0] * math.laplace.Em_minus(i, x, y);
				}
				return 1 - sum;
			};

			Func<double, double, double, double> val = (x, y, t) =>
				phiN(x, y) * math.laplace.E_minus(x, y, math.contour.x(t), math.contour.y(t));

			if (!pass)
			{
				Func<double, double, double> Roben = (x, y) =>
					math.integral.calc(
						0,
						2 * Math.PI,
						(t) => val(x, y, t)
					) * math.settings.needCalcPointOutside(x, y);
				CalcSurf(Roben, math.settings.x1 * 1.2, math.settings.y1(0) * 1.2, prefix, math.settings.sufrSteps);
			}
			return phiN;
		}

		private static Func<double, double, double> calcSelfVortex(Func<double, double, double> Roben, bool pass)
		{
			// Здесь решается задача для бигармонического уравнения
			Console.WriteLine("processing Ax=b:");
			string prefix = "vortex_";
			math.Matrix cm;
			if (!pass)
			{
				string B = "";
				StringBuilder sb = new StringBuilder();
				for (int k = 0; k < math.settings.zN; k++)
				{
					for (int p = 0; p < math.settings.zN; p++)
					{
						// Console.WriteLine("k:{0} p:{1}", k, p);
						Func<double, double, double> f = (x, y) => math.laplace.Em(k, x, y) * math.laplace.Em(p, x, y);
						double v = math.integral2.calc(
							math.settings.x1,
							math.settings.x2,
							math.settings.y1,
							math.settings.y2,
							f,
							math.settings.needCalcPointInside
						);
						if (p < math.settings.zN - 1)
						{
							sb.Append(v + " ");
						}
						else
						{
							sb.Append(v);
						}
					}

					Func<double, double> b = (t) => 
						Roben(math.contour.x(t), math.contour.y(t)) *math.laplace.Em(k, math.contour.x(t), math.contour.y(t));
					double valB = - math.integral.calc(0, 2 * Math.PI, b);
					
					if (k < math.settings.zN - 1)
					{
						sb.AppendLine();
						B += valB + " ";
					}
					else
					{
						B += valB;
					}

					Console.CursorLeft = 0;
					Console.Write("{0}%", 100 * k / math.settings.zN);
				}
				Console.CursorLeft = 0;
				Console.WriteLine("100%");
				System.IO.File.WriteAllText(prefix + "A.data", sb.ToString());
				System.IO.File.WriteAllText(prefix + "B.data", B.ToString());

				math.Matrix A = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "A.data"));
				math.Matrix B_vector = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "B.data"));
				cm = A.SolveWith(math.Matrix.Transpose(B_vector));
				System.IO.File.WriteAllText(prefix + "cm.data", cm.ToString());
			}
			else
			{
				cm = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "cm.data"));
			}

			Func<double, double, double> gStar = (x, y) =>
			{
				double sum = 0;
				for (int i = 0; i < math.settings.zN; i++)
				{
					sum += cm[i, 0] * math.laplace.Em(i, x, y);
				}
				return sum;
			};

			if (!pass)
			{
				Func<double, double, double> U = (x, y) =>
					math.settings.needCalcPointInside(x, y) * 
					(
						math.integral2.calc(
							math.settings.x1,
							math.settings.x2,
							math.settings.y1,
							math.settings.y2,
							(r, t) => gStar(r, t) * math.laplace.E(x, y, r, t),
							math.settings.needCalcPointInside
						)
						+ math.integral.calc(0, 2 * Math.PI, (t) =>
							Roben(math.contour.x(t), math.contour.y(t)) * math.laplace.E(x, y, math.contour.x(t), math.contour.y(t))
						)
					);

				CalcSurf(U, math.settings.x1*1.2, math.settings.y1(0)*1.2, prefix, math.settings.sufrSteps);
			}
			return gStar;
		}

		private static Func<double, double, double> calcPsi(Func<double, double, double> gStar)
		{

			Console.WriteLine("processing Ax=b:");
			string prefix = "psi_";
			string B = "";
			// math.mu.loadMuMs(prefix);

			StringBuilder sb = new StringBuilder();
			for (int k = 0; k < math.settings.zN; k++)
			{
				for (int p = 0; p < math.settings.zN; p++)
				{
					// Console.WriteLine("k:{0} p:{1}", k, p);
					/*
					Func<double, double, double> f = (x, y) => math.mu.calcM(k, x, y) * math.mu.calcM(p, x, y);
					double v = math.integral2.calc(
						math.settings.x1,
						math.settings.x2,
						math.settings.y1,
						math.settings.y2,
						f,
						math.settings.needCalcPointInside
					);
					/**/

					Func<double, double> f = (t) => 
						math.mu.calcM(k, math.contour.x(t), math.contour.y(t)) *
						math.mu.calcM(p, math.contour.x(t), math.contour.y(t));
					double v = math.integral.calc(0, 2 * Math.PI, f);
					/**/
					if (p < math.settings.zN - 1)
					{
						sb.Append(v + " ");
					}
					else
					{
						sb.Append(v);
					}
					Console.CursorLeft = 0;
					Console.Write("{0}% of {1} iteration", 100 * p / math.settings.zN, k);
				}
				Console.WriteLine();
				/*
				Func<double, double, double> b = (x, y) => math.f.calc(x, y) * math.mu.calcM(k, x, y);
				double valB = math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					math.settings.y1,
					math.settings.y2,
					b,
					math.settings.needCalcPointInside
				);
				/**/
				
				Func<double, double> b = (t) => 
					math.f.calc(t) * 
					math.mu.calcM(k, math.contour.x(t), math.contour.y(t));
				double valB = math.integral.calc(0, 2 * Math.PI, b);
				/**/
				if (k < math.settings.zN - 1)
				{
					sb.AppendLine();
					B += valB + " ";
				}
				else
				{
					B += valB;
				}
				// Console.WriteLine("B={0}", B);
				Console.CursorLeft = 0;
				Console.Write("{0}%", 100 * k / math.settings.zN);

			}
			Console.CursorLeft = 0;
			Console.WriteLine("100%");
			System.IO.File.WriteAllText(prefix+"A.data", sb.ToString());
			System.IO.File.WriteAllText(prefix+"B.data", B.ToString());

			math.Matrix A = math.Matrix.Parse(System.IO.File.ReadAllText(prefix+"A.data"));
			math.Matrix B_vector = math.Matrix.Parse(System.IO.File.ReadAllText(prefix+"B.data"));
			math.Matrix cm = A.SolveWith(math.Matrix.Transpose(B_vector));
			System.IO.File.WriteAllText(prefix + "cm.data", cm.ToString());
			// math.mu.saveMuMs(prefix);

			Func<double, double, double> gM = (x, y) =>
			{
				double sum = 0;
				for (int i = 0; i < math.settings.zN; i++)
				{
					sum += cm[i, 0] * math.laplace.Em(i, x, y);
				}
				return sum;
			};

			Func<double> C = () =>
			{
				double a = math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					math.settings.y1,
					math.settings.y2,
					(r, t) => gM(r, t) * gStar(r, t),
					math.settings.needCalcPointInside
				);

				double b = math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					math.settings.y1,
					math.settings.y2,
					(r, t) => gStar(r, t) * gStar(r, t),
					math.settings.needCalcPointInside
				);

				return -a / b;
			};
			double c = C();

			Func<double, double, double> g0M = (x, y) =>
			{
				return gM(x, y) + c * gStar(x, y);
			};
			
			Func<double, double, double> Psi = (x, y) =>
				math.settings.needCalcPointInside(x, y) *
				math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					math.settings.y1,
					math.settings.y2,
					(r, t) => gM(r, t) * math.laplace.E(x, y, r, t),
					math.settings.needCalcPointInside
				);
			CalcSurf(Psi, math.settings.x1*1.2, math.settings.y1(0)*1.2, prefix, math.settings.sufrSteps);
			return Psi;
		}

		private static Func<double, double, double> calcPsiSimple()
		{

			Console.WriteLine("processing Ax=b:");
			string prefix = "psiSimple_";
			string B = "";

			StringBuilder sb = new StringBuilder();
			for (int k = 0; k < math.settings.zN; k++)
			{
				sb.AppendLine();
				for (int p = 0; p < math.settings.zN; p++)
				{
					// Console.WriteLine("k:{0} p:{1}", k, p);
					Func<double, double, double> f = (x, y) => math.mu.calcM(k, x, y) * math.mu.calcM(p, x, y);
					double v = math.integral2.calc(
						math.settings.x1,
						math.settings.x2,
						math.settings.y1,
						(z) => 0,
						f,
						math.settings.needCalcPointInside
					) +
					math.integral2.calc(
						math.settings.x1,
						math.settings.x2,
						(z) => 0,
						math.settings.y2,
						f,
						math.settings.needCalcPointInside
					);
					sb.Append(v + " ");
					Console.CursorLeft = 0;
					Console.Write("{0}% of {1} iteration", 100 * p / math.settings.zN, k);
				}
				Console.WriteLine();
				Func<double, double> b = (t) => math.f.calc(t) * math.mu.calcM(k, math.contour.x(t), math.contour.y(t));
				double valB = math.integral.calc(0, 2 * Math.PI, b);
				B += valB + " ";
				Console.CursorLeft = 0;
				Console.Write("{0}%", 100 * k / math.settings.zN);

			}
			Console.CursorLeft = 0;
			Console.WriteLine("100%");
			System.IO.File.WriteAllText(prefix + "A.data", sb.ToString());
			System.IO.File.WriteAllText(prefix + "B.data", B.ToString());

			math.Matrix A = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "A.data"));
			math.Matrix B_vector = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "B.data"));
			math.Matrix cm = A.SolveWith(math.Matrix.Transpose(B_vector));
			System.IO.File.WriteAllText(prefix + "cm.data", cm.ToString());

			Func<double, double, double> gM = (x, y) =>
			{
				double sum = 0;
				for (int i = 0; i < math.settings.zN; i++)
				{
					sum += cm[i, 0] * math.laplace.Em(i, x, y);
				}
				return sum;
			};

			Func<double, double, double> Psi = (x, y) =>
				math.settings.needCalcPointInside(x, y) * (
				math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					math.settings.y1,
					z => 0,
					(r, t) => gM(x, y) * math.laplace.E(x, y, r, t),
					math.settings.needCalcPointInside
				) +
				math.integral2.calc(
					math.settings.x1,
					math.settings.x2,
					z => 0,
					math.settings.y2,
					(r, t) => gM(x, y) * math.laplace.E(x, y, r, t),
					math.settings.needCalcPointInside
				))
				;
			CalcSurf(Psi, math.settings.x1, math.settings.y1(0), prefix, math.settings.sufrSteps);
			return Psi;
		}

		static void view_onReady(object sender, EventArgs e)
		{
			view.clear();
			view.calcConv(15, 10);

			List<double> xz = new List<double>(math.settings.zx);
			List<double> yz = new List<double>(math.settings.zy);
			view.drawPolydot(xz, yz, "Red", 2);

			List<double> xz_minus = new List<double>(math.settings.zx_minus);
			List<double> yz_minus = new List<double>(math.settings.zy_minus);
			view.drawPolydot(xz_minus, yz_minus, "Green", 2);

			List<double> xs = new List<double>(math.settings.sx);
			List<double> ys = new List<double>(math.settings.sy);
			view.drawPolyline(xs, ys, "Black");

			List<double> xzl = new List<double>(psix_layers_values);
			List<double> yzl = new List<double>(psiy_layers_values);
			view.drawPolydot(xzl, yzl, "Blue", 1);
		}

		static void coupleSolo()
		{
			Console.Beep(220, 400);
		}

		static void refrenSolo()
		{
			Console.Beep(440, 400);
		}

		/// <summary>
		/// решается задача для бигармонического уравнения
		/// </summary>
		/// <param name="f"></param>
		/// <param name="pass"></param>
		/// <returns></returns>
		private static Func<double, double, double> calcBiharm(Func<double, double> U, Func<double, double> dUdn, bool pass)
		{
			// Здесь решается задача для бигармонического уравнения
			Console.WriteLine("processing Ax=b:");
			string prefix = "biharm_";
			math.Matrix cm;
			if (!pass)
			{
				string B = "";
				StringBuilder sb = new StringBuilder();
				for (int k = 0; k < math.settings.zN; k++)
				{
					for (int p = 0; p < math.settings.zN; p++)
					{
						// Console.WriteLine("k:{0} p:{1}", k, p);
						Func<double, double, double> f = (x, y) => math.laplace.Em(k, x, y) * math.laplace.Em(p, x, y);
						double v = math.integral2.calc(
							math.settings.x1,
							math.settings.x2,
							math.settings.y1,
							math.settings.y2,
							f,
							math.settings.needCalcPointInside
						);
						if (p < math.settings.zN - 1)
						{
							sb.Append(v + " ");
						}
						else
						{
							sb.Append(v);
						}
					}

					Func<double, double> b = (t) =>
						U(t) * math.laplace.dEmdn(k, math.contour.x(t), math.contour.y(t)) - dUdn(t) * math.laplace.dEmdn(k, math.contour.x(t), math.contour.y(t));

					double valB = -math.integral.calc(0, 2 * Math.PI, b);

					if (k < math.settings.zN - 1)
					{
						sb.AppendLine();
						B += valB + " ";
					}
					else
					{
						B += valB;
					}

					Console.CursorLeft = 0;
					Console.Write("{0}%", 100 * k / math.settings.zN);
				}
				Console.CursorLeft = 0;
				Console.WriteLine("100%");
				System.IO.File.WriteAllText(prefix + "A.data", sb.ToString());
				System.IO.File.WriteAllText(prefix + "B.data", B.ToString());

				math.Matrix A = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "A.data"));
				math.Matrix B_vector = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "B.data"));
				cm = A.SolveWith(math.Matrix.Transpose(B_vector));
				System.IO.File.WriteAllText(prefix + "cm.data", cm.ToString());
			}
			else
			{
				cm = math.Matrix.Parse(System.IO.File.ReadAllText(prefix + "cm.data"));
			}

			Func<double, double, double> gStar = (x, y) =>
			{
				double sum = 0;
				for (int i = 0; i < math.settings.zN; i++)
				{
					sum += cm[i, 0] * math.laplace.Em(i, x, y);
				}
				return sum;
			};

			if (!pass)
			{
				Func<double, double, double> U = (x, y) =>
					math.settings.needCalcPointInside(x, y) *
					(
						math.integral2.calc(
							math.settings.x1,
							math.settings.x2,
							math.settings.y1,
							math.settings.y2,
							(r, t) => gStar(r, t) * math.laplace.E(x, y, r, t),
							math.settings.needCalcPointInside
						)
						+ math.integral.calc(0, 2 * Math.PI, (t) =>
							dUdn(t) * math.laplace.E(x, y, math.contour.x(t), math.contour.y(t))
						)
					);

				CalcSurf(U, math.settings.x1 * 1.0, math.settings.y1(0) * 1.0, prefix, math.settings.sufrSteps);
			}
			return gStar;
		}

	}
}
