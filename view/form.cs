﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace view
{
	public partial class form : Form
	{
		public event EventHandler onReady;
		Graphics g;
		public form()
		{
			InitializeComponent();
		}
		double scale = 1;
		int x0 = 0;
		int y0 = 0;
		public void drawPolyline(List<double> xs, List<double> ys, string color)
		{
			for (int i = 0; i < xs.Count-1; i++)
			{
				int x1 = getX(xs[i]);
				int y1 = getY(ys[i]);
				int x2 = getX(xs[i+1]);
				int y2 = getY(ys[i+1]);
				Color cl = Color.FromName(color);
				using(Pen p = new Pen(cl))
				{
					g.DrawLine(p, new Point(x1, y1), new Point(x2, y2));
				}
			}
		}

		public void drawPolydot(List<double> xs, List<double> ys, string color, int radius)
		{
			for (int i = 0; i < xs.Count - 1; i++)
			{
				int x1 = getX(xs[i]);
				int y1 = getY(ys[i]);
				int x2 = getX(xs[i + 1]);
				int y2 = getY(ys[i + 1]);
				Color cl = Color.FromName(color);
				using (Pen p = new Pen(cl))
				{
					g.DrawEllipse(p, x1, y1, (int)(radius * 2), (int)(radius * 2));
				}
			}
		}

		public void clear()
		{
			g.FillRectangle(Brushes.White, 0, 0, pb.Width, pb.Height);
		}

		public void calcConv(double w, double h) {
			x0 = this.pb.Width / 2;
			y0 = this.pb.Height / 2;
			double scalex = (1.0*this.pb.Width) / w;
			double scaley = (1.0*this.pb.Height) / h;

			this.scale = Math.Min(scalex, scaley);
		}

		int getX(double x) {
			return (int)(x * scale) + x0;
		}

		int getY(double y)
		{
			return (int)(this.pb.Height - y * scale) - y0;
		}

		private void form_Shown(object sender, EventArgs e)
		{

		}

		private void pb_Paint(object sender, PaintEventArgs e)
		{
			g = e.Graphics;
			var handler = onReady;
			if (handler != null)
			{
				handler(this, null);
			}
			//g.DrawLine(Pens.Black, new Point(0, 0), new Point(100, 100));
		}
	}
}
