﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class mu
	{
		// private static readonly object valuesLock = new object();
		// private static ConcurrentDictionary<string, double> values = new ConcurrentDictionary<string, double>();
		private static string muMFileName = "muM.data";
		public static void loadMuMs(string prefix)
		{
			values = file.Read(muMFileName, prefix);
		}
		public static void saveMuMs(string prefix)
		{
			file.Write(values, muMFileName, prefix);
		}
		private static Dictionary<string, double> values = new Dictionary<string, double>();
		public static double calc(double x1, double y1, double x2, double y2)
		{
			Func<double, double, double> f = (x, y) => laplace.E(x1, y1, x, y) * laplace.E(x2, y2, x, y);
			double v = integral2.calc(settings.x1, settings.x2, settings.y1, settings.y2, f, math.settings.needCalcPointInside);
			//values.TryAdd(key, v);
			return v;
		}

		public static double calcM(int m, double x2, double y2)
		{
			/*
			string key = getKey(m, x2, y2);
			if (values.ContainsKey(key))
			{
				return values[key];
			}
			/**/
			double v = calc(settings.zx[m], settings.zy[m], x2, y2);
			/*
			values.Add(key, v);
			if (values.Count % 1000 == 0)
			{
				Console.WriteLine(key);
			}
			/**/
			return v;
		}

		private static string getKey(int m, double x2, double y2)
		{
			string key = m.ToString() + "|" + x2.ToString("F1") + "|" + y2.ToString("F1");
			return key;
		}
	}
}
