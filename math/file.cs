﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class file
	{
		public static void Write(Dictionary<string, double> dictionary, string file, string prefix)
		{
			file = prefix + file;
			if (File.Exists(file))
			{
				File.Delete(file);
				File.Create(file);
			}
			using (FileStream fs = File.OpenWrite(file))
			using (BinaryWriter writer = new BinaryWriter(fs))
			{
				// Put count.
				writer.Write(dictionary.Count);
				// Write pairs.
				foreach (var pair in dictionary)
				{
					writer.Write(pair.Key);
					writer.Write(pair.Value);
				}
			}
		}

		public static Dictionary<string, double> Read(string file, string prefix)
		{
			file = prefix + file;
			var result = new Dictionary<string, double>();
			if (File.Exists(file))
			{
				using (FileStream fs = File.OpenRead(file))
				using (BinaryReader reader = new BinaryReader(fs))
				{
					// Get count.
					int count = reader.ReadInt32();
					// Read in all pairs.
					for (int i = 0; i < count; i++)
					{
						string key = reader.ReadString();
						string value = reader.ReadString();
						double vd = 0;
						if (double.TryParse(value, out vd))
						{
							result[key] = vd;
						}
					}
				}
			}
			return result;
		}
	}
}
