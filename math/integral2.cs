﻿using MathNet.Numerics.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class integral2
	{
		private static int steps = settings.steps;
		private static double eps = settings.eps;
		private static double calcq(double x1, double x2, Func<double, double> y1, Func<double, double> y2, Func<double, double, double> f, Func<double, double, int> needCalcPointInside)
		{
			return calc(x1, x2, y1, y2, f, needCalcPointInside);

			/*
			int len = (int)Math.Floor((x2-x1) * steps);
			double step = 1.0 / steps;
			double sum = 0;
			double x = x1;
			for (int i = 0; i < len; i++)
			{
				sum += integral.calcq(y1(x), y2(x), y => f(x,y));
				x += step;
			}
			return sum*step;
			/**/
		}

		public static double calc(double x1, double x2, Func<double, double> y1, Func<double, double> y2, Func<double, double, double> f, Func<double, double, int> needCalcPointInside)
		{

			// 2D integration using a 5-point Gauss-Legendre rule over the integration interval [0, 10] X [1, 2]
			double integrate2D = GaussLegendreRule.Integrate((x, y) => f(x, y) * needCalcPointInside(x, y), x1, x2, y1(0), y2(0), math.settings.steps);
			return integrate2D;

			/*
			double len = (x2 - x1);
			double sum = 0;
			double a = x1;
			double b = x1 + len / 3;
			double c = x1 + 2 * len / 3;
			double d = x1 + len;
			Func<double, double> g = u => integral.calc(y1(u), y2(u), y => f(u, y));

			if (Math.Abs(x1 + x2) < eps)
			{
				sum = g(Math.Sign(x1 + x2) * eps) * len;
			}
			else
			{
				bool r1 = Math.Abs(g(a) - g(b)) < eps;
				bool r2 = Math.Abs(g(b) - g(c)) < eps;
				bool r3 = Math.Abs(g(c) - g(d)) < eps;
				bool r4 = Math.Abs(len) < eps;
				if (r1 && r2 && r3 || r4)
				{
					sum = g((x1 + x2) / 2) * len;
					if (double.IsNaN(sum) || double.IsInfinity(sum))
					{
						Console.WriteLine("news!");
					}
				}
				else
				{
					double step = len / 4;
					for (int k = 0; k < 4; k++)
					{
						sum += calc(x1 + k * step, x1 + (k + 1) * step, y1, y2, f);
					}
				}
			}
			return sum;
			/**/
		}
	}
}
