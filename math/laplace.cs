﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
    public static class laplace
    {
		static double eps = 1E-1;
		public static double E(double x1, double y1, double x2, double y2)
		{
			
			double v = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
			// return Math.Log(v) / 2 / Math.PI;
			if (v < eps)
			{
				return -9.21034037197618;
			}
			return Math.Log(v) / 2 / Math.PI;
		}
		public static double E_minus(double x1, double y1, double x2, double y2)
		{

			double v = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
			// return Math.Log(v) / 2 / Math.PI;
			if (v < eps)
			{
				return -9.21034037197618;
			}
			return Math.Log(v) / 2 / Math.PI;
		}

		public static double Em(int m, double x2, double y2)
		{

			double v = Math.Sqrt(Math.Pow(settings.zx[m] - x2, 2) + Math.Pow(settings.zy[m] - y2, 2));
			// return Math.Log(v) / 2 / Math.PI;
			if (v < eps)
			{
				return -9.21034037197618;
			}
			return Math.Log(v) / 2 / Math.PI;
		}

		public static double dEmdn(int m, double x2, double y2)
		{
			double v = (Math.Pow(settings.zx[m] - x2, 2) + Math.Pow(settings.zy[m] - y2, 2));
			if (v > 1E5)
			{
				return 0;
			}

			double cosa = 0;
			double cosb = 1;
			double dx = (settings.zx[m] - x2) / (Math.Pow(settings.zx[m] - x2, 2) + Math.Pow(settings.zy[m] - y2, 2));
			double dy = (settings.zy[m] - y2) / (Math.Pow(settings.zx[m] - x2, 2) + Math.Pow(settings.zy[m] - y2, 2));
			double res = dx * cosa + dy * cosb;

			return res;
		}

		public static double Em_minus(int m, double x2, double y2)
		{

			double v = Math.Sqrt(Math.Pow(settings.zx_minus[m] - x2, 2) + Math.Pow(settings.zy_minus[m] - y2, 2));
			// return Math.Log(v) / 2 / Math.PI;
			if (v < eps)
			{
				return -9.21034037197618;
			}
			return Math.Log(v) / 2 / Math.PI;
		}
    }
}
