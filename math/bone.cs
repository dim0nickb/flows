﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class bone
	{
		public static Func<double, double> x = (t) => 3 * Math.Cos(t);

		public static Func<double, double> y = (t) => 3 * Math.Sin(t) + 3;
	}
}
