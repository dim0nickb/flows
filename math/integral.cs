﻿using MathNet.Numerics.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class integral
	{
		private static Dictionary<string, double> values = new Dictionary<string, double>();
		public static void saveValues(string filename) {
			List<string> vals = new List<string>();
			foreach (var item in values)
			{
				vals.Add(item.Key + "|>" + item.Value);
			}
			System.IO.File.WriteAllLines(filename, vals.ToArray());
		}
		public static void resetValues()
		{
			values = new Dictionary<string, double>();
		}
		private static int steps = settings.steps;
		private static double eps = settings.eps;
		private static double calcq(double x1, double x2, Func<double, double> f)
		{
			return calc(x1, x2, f);
			/*
			int len = (int)Math.Floor((x2-x1) * steps);
			double step = 1.0 / steps;
			double sum = 0;
			double x = x1;
			for (int i = 0; i < len; i++)
			{
				sum += f(x);
				x += step;
			}
			double value = sum*step;

			// values.Add(key, value);

			return value;
			/**/
		}

		public static double calc(double x1, double x2, Func<double, double> f)
		{
			// 1D integration using a 5-point Gauss-Legendre rule over the integration interval [0, 10]
			double integrate1D = GaussLegendreRule.Integrate(f, x1, x2, math.settings.steps);
			return integrate1D;
			
			/*
			string key = getKey(x1, x2, f);
			if (values.ContainsKey(key))
			{
				return values[key];
			}
			double len = (x2 - x1);
			double sum = 0;
			double a = x1;
			double b = x1 + len / 3;
			double c = x1 + 2 * len / 3;
			double d = x1 + len;

			{
				bool r1 = Math.Abs(f(a) - f(b)) < eps;
				bool r2 = Math.Abs(f(b) - f(c)) < eps;
				bool r3 = Math.Abs(f(c) - f(d)) < eps;
				bool r4 = Math.Abs(len) < eps;
				if (r1 && r2 && r3 || r4)
				{
					sum = (f(x1) + f(x2)) / 2 * len;
					if (double.IsNaN(sum) || double.IsInfinity(sum))
					{
						Console.WriteLine("news!");
					}
				}
				else
				{
					double step = len / 4;
					for (int k = 0; k < 4; k++)
					{
						sum += calc(x1 + k * step, x1 + (k + 1) * step, f);
					}
				}
			}
			values.Add(key, sum);
			
			return sum;
			/**/
		}

		private static string getKey(double x1, double x2, Func<double, double> f)
		{
			string key = f(1).ToString("F4") + "|" + f(1.5).ToString("F4") + "|" + f(2).ToString("F4") + "|" + x1.ToString("F4") + "|" + x2.ToString("F4");
			return key;
		}
	}
}
