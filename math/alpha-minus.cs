﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class alpha_minus
	{
		public static double calcM(int m, double x2, double y2)
		{
			if (m < settings.zN - 1)
			{
				return laplace.Em_minus(m + 1, x2, y2) - laplace.Em_minus(m, x2, y2);
			}
			else
			{
				return laplace.Em_minus(0, x2, y2) - laplace.Em_minus(settings.zN-1, x2, y2);
			}
		}
	}
}
