﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class settings
	{
		public static readonly double eps = 1E-1;
		public static readonly int steps = 256; // Approximates a definite integral using an Nth order Gauss-Legendre rule. Precomputed Gauss-Legendre abscissas/weights for orders 2-20, 32, 64, 96, 100, 128, 256, 512, 1024 are used, otherwise they're calculated on the fly.
		public static readonly int sufrSteps = 200;
		public static readonly double x1 = -5;
		public static readonly double x2 = 5;
		public static readonly Func<double, double> y1 = (x) => -3 * Math.Sqrt(1 - x * x / 25);
		public static readonly Func<double, double> y2 = (x) =>  3 * Math.Sqrt(1 - x * x / 25);

		public static readonly int zN = 10;
		public static readonly int zN_inner = 0;
		public static List<double> zx = new List<double>(zN);
		public static List<double> zy = new List<double>(zN);
		public static List<double> zx_minus = new List<double>(zN);
		public static List<double> zy_minus = new List<double>(zN);

		public static readonly int sN = 360;
		public static List<double> sx = new List<double>(zN);
		public static List<double> sy = new List<double>(zN);

		public static Func<double, double, int> needCalcPointInside = (x_, y_) =>
		{
			/*
			double dx = 1.5;
			double dy = 1;

			double x = x_ + dx;
			double y = y_ - 3 + dy;
			bool isBone =  x * x / 4 + y * y / 4 < 1 && x > 0 + dx && x < 0.5 && y > 0 && y < 2 && x * x / 1 + y * y / 1 > 1;

			/*
			// SQUARE
			bool isBone = x_ < 0.5 && x_ > -0.1 && y_ < 3 && y_ > 1;
			/*
			bool isBone = x_ * x_ / 0.09 + (y_-2) *( y_-2) / 1 < 1;
			/**/
			/*
			double x = x_ + 1;
			double y = y_ - 1;
			double a = 0.3;
			Func<double, double> cos = (t) => Math.Cos(t);
			Func<double, double> sin = (t) => Math.Sin(t);
			Func<double, double, double> pow = (r, t) => Math.Pow(r, t);
			bool isBone = (pow(x * cos(a) - y * sin(a), 2)) / 0.01 + (pow(x * sin(a) + y * cos(a), 2)) / 4 < 1;
			/**/
			bool res = x_ * x_ / 25 + y_ * y_ / 9 < 1; // && !isBone;
			return res ? 1 : 0;
		};
		public static Func<double, double, int> needCalcPointOutside = (x, y) =>
		{
			int res = needCalcPointInside(x, y);
			return res == 1 ? 0 : 1;
		};
	}
}
