﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace math
{
	public static class contour
	{
		public static Func<double, double> x = (t) => 5 * Math.Cos(t);

		public static Func<double, double> y = (t) => 3 * Math.Sin(t);

		public static Func<double, double> xt = (t) => -5 * Math.Sin(t);

		public static Func<double, double> yt = (t) => 3 * Math.Cos(t);

		public static Func<double, double> ft = (t) => yt(t)/xt(t);
	}
}
